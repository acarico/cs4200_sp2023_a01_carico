# CS4200_SP2023_A01_CARICO



## Austin Carico

### Bachelor of Science in Computer Science, specialized in Computational Data Science

<b> What is your technical background? </b> <br>
I have taken several courses at BGSU in Computer Science, learning alot about C++ and Python. I am somewhat comfortable with Python, notably when I code in Jupyter Notebook, but in terms of overall languages, I am most comfortable with SQL as I have been regularly using it for my current internship. Statistics wise I have taken Probability and Statistics I and am currently taking the follow up to that course, Probability and Statistics II. I think I am weaker in the statistics field since I have only taken these courses. My ongoing internship is in Data Analytics, and it primarily uses SQL and Powerbi, so those are my biggest technical strengths at the moment. I have not used any big data tools like Spark or Hadoop before. <br> <br>

<b> What most concerns you about this course? </b> <br>
I think I'll struggle with some of the higher level statistics required in this course since I don't feel too well equipped to handle some of the higher level processes that are used in Artificial Intelligence. I also think (but I do not know) that I may struggle learning some newer big data technologies, but I look forward to learning them the most. <br> <br>

<b> Are there things that will help you succeed in this course? </b> <br>
I think there are several resources available for helping me learn some of the newer technologies and methods. Using common sites like tutorialspoint and w3schools would help me along with the textbook.

<b> Is there anything else you would like the instructor to know? </b> <br>
Throughout the course when we learn any further big data analytics technologies, I would love additional sources to use that would help us understand some of the newer concepts
<br> <br>
<b> In your opinion, what is AI? what can AI do? what do you want to do with AI? </b> <br>
I think AI is technology that is able to read in loads of input and then make decisions based of that input. I think AI can adapt to its environment and make decisions based off of input from this environment. I would like AI to be able to make complex processes in computing more simple, notably educational processes, to ease the pressure on teachers and educators.
